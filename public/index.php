<?php

require __DIR__ . '/../vendor/autoload.php';

use lib\DBClass;

session_cache_limiter(false);
session_start();

$app = new \Slim\Slim(array(
    'mode' => 'development',
    'cookies.lifetime' => '5 minutes',
    'view' => new \Slim\Views\Twig()
));

// Only invoked if mode is "production"
$app->configureMode('production', function () use ($app) {
    $app->config(array(
        'log.enable' => true,
        'debug' => false
    ));
});

// Only invoked if mode is "development"
$app->configureMode('development', function () use ($app) {
    $app->config(array(
        'log.enable' => false,
        'debug' => true,
        'cookies.domain' => 'emp-blog.local'
    ));
});

// Use custom DB class
$app->container->singleton('db', function () {
    return new DBClass();
});

// Set Twig options
$view = $app->view();
$view->parserDirectory = 'Twig';
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/../cache/templates'
);
$view->setTemplatesDirectory(__DIR__.'/../templates/');

// twig helper functions
$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
);

// Register routes
require __DIR__ . '/../src/routes.php';
require __DIR__ . '/../src/admin.php';

// Run app
$app->run();
