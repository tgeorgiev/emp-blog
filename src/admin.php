<?php

use lib\PostClass;
use lib\UserClass;

$isAuthenticated = function () use ($app) {

    if(!isset($_SESSION['user']['isLogged'])) {
        $app->redirect('/auth/login');
    }

    $now = time();
    if (isset($_SESSION['discard_after']) && $now > $_SESSION['discard_after']) {
        // more than 5 min have past
        session_unset();
        session_destroy();

        $app->redirect('/auth/login');
    }

    // renew with 5 min
    $_SESSION['discard_after'] = $now + 300;
};

// Admin group
$app->group('/admin', $isAuthenticated, function () use ($app) {

    $app->get('/', function () use ($app) {
        $postDB = new PostClass();
        $userDB = new UserClass();
        $posts = $postDB->findLatest();
        $users = $userDB->findLatest();
        $app->render('admin/dashboard.twig', [
            'posts' => $posts,
            'users' => $users
        ]);
    })->name('admin_dashboard');

    // Post group
    $app->group('/post', function () use ($app) {

        $app->get('/', function () use ($app) {
            $postDB = new PostClass();
            $posts = $postDB->findAll();
            $app->render('admin/post_index.twig', [
                'posts' => $posts
            ]);
        })->name('admin_post_index');

        // new post form
        $app->get('/new', function () use ($app) {
            $app->render('admin/post_new.twig');
        });

        // create new post
        $app->post('/new', function () use ($app) {
            $data = array(
                'title' => $app->request->post('title'),
                'content' => $app->request->post('content'),
            );

            $postDB = new PostClass();
            $postDB->add($data);

            $app->redirect('/admin/post/');
        });

        // edit post form
        $app->get('/edit/:id', function ($id) use ($app) {
            $postDB = new PostClass();
            $post = $postDB->find($id);
            $app->render('admin/post_edit.twig', [
                'post' => $post
            ]);
        });

        // update post
        $app->put('/:id', function ($id) use ($app) {

            $data = array(
                'id' => $id,
                'title' => $app->request->put('title'),
                'content' => $app->request->put('content'),
            );

            try {
                $postDB = new PostClass();
                $postDB->update($data);
            } catch(Exception $e) {
                $app->flash('error', $e->getMessage());
            }
            $app->redirect('/admin/post/');

        });

        // delete post
        $app->delete('/:id', function ($id) use ($app) {
            $postDB = new PostClass();
            $postDB->delete($id);

            $app->redirect('/admin/post/');
        });
    });

    // User group
    $app->group('/user', function () use ($app) {

        $app->get('/', function () use ($app) {
            $userDB = new UserClass();

            $app->render('admin/user_index.twig', [
                'users' => $userDB->findAll()
            ]);
        })->name('admin_user_index');

        // new user form
        $app->get('/new', function () use ($app) {
            $app->render('admin/user_new.twig');
        });

        // create new user
        $app->post('/new', function () use ($app) {
            $data = array(
                'username' => $app->request->post('username'),
                'password' => password_hash($app->request->post('password'), PASSWORD_BCRYPT),
            );

            $userDB = new UserClass();
            $userDB->add($data);

            $app->redirect('/admin/user/');
        });

        // edit user form
        $app->get('/edit/:id', function ($id) use ($app) {
            $userDB = new UserClass();
            $user = $userDB->find($id);
            $app->render('admin/user_edit.twig', [
                'user' => $user
            ]);
        });

        // update user
        $app->put('/:id', function ($id) use ($app) {

            $data = array(
                'id' => $id,
                'username' => $app->request->put('username'),
                'password' => $app->request->put('password'),
            );

            try {
                $userDB = new UserClass();
                $userDB->update($data);
            } catch(Exception $e) {
                $app->flash('error', $e->getMessage());
            }
            $app->redirect('/admin/user/');

        });

        // delete user
        $app->delete('/:id', function ($id) use ($app) {
            $userDB = new UserClass();
            $userDB->delete($id);

            $app->redirect('/admin/user/');
        });
    });
});