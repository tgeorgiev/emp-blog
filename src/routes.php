<?php
use lib\PostClass;
use lib\UserClass;

$app->get('/', function () use ($app) {
    $postDB = new PostClass();
    $posts = $postDB->findAll();
    $app->render('blog_index.twig', [
        'posts' => $posts
    ]);
});

$app->post('/post/:id', function ($id) use ($app) {

    $data = array(
        'id'        => $id,
        'title'     => $app->request()->post('title'),
        'content'   => $app->request()->post('content')
    );

    $postDB = new PostClass();

    try {
        $postDB->edit($data);
    } catch(\Exception $e) {
        $app->flash('error', $e->getMessage());
        $app->redirect('/error');
    }
    $app->redirect('/');
});

// Authentication group
$app->group('/auth', function () use ($app) {

    $app->get('/login', function() use ($app) {
        $app->render('login.twig');
    })->name('auth.login');

    $app->get('/logout', function() use ($app) {
        unset($_SESSION['user']);
        $app->redirect('/');
    })->name('auth.logout');

    // Check user authentication
    $app->post('/login', function() use ($app) {

        $data = array(
            'user'     => $app->request()->post('username'),
            'pass'     => $app->request()->post('password')
        );

        $userDB = new UserClass();

        if($userDB->checkCredentials($data)) {
            $_SESSION['user']['isLogged'] = true;
            $_SESSION['user']['username'] = $app->request()->post('username');
            $app->redirect('/admin/post/');
        }

        $app->render('login.twig');
    });
});