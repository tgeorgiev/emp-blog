-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2018 at 08:58 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emp-blog`
--
CREATE DATABASE IF NOT EXISTS `emp-blog` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `emp-blog`;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Bootstrap 4.1.2', 'We’ve been busy these last couple months since launching v4.1.1, but we’re back with another bug fix and some sweeping changes to how we build and publish our docs after the issues stemming from our v4.1.x launches.\r\n\r\nWhen we launched v4.1, we ran into unexpected issues with having to rearrange asset paths after deploying, resulting in broken image URLs, a busted service worker, and more. Since then, we’re ironed out most of the kinks and introduced a new docs directory structure inside the repo. Nothing should change for anyone using our docs, but those contributing to the project and developing locally may need to rebase their changes or update their branches accordingly.\r\n\r\nBeyond the file structure changes, here are the highlights for v4.1.2:\r\n\r\nFixed an XSS vulnerability in tooltip, collapse, and scrollspy plugins\r\nImproved how we query elements in our JavaScript plugins\r\nInline SVGs now have the same vertical alignment as images\r\nFixed issues with double transitions on carousels\r\nAdded Edge and IE10-11 fallbacks to our floating labels example\r\nVarious improvements to form controls, including disabled states on file inputs and unified focus styles for selects\r\nMiscellaneous build tool improvements and documentation fixes', '2018-09-17 22:34:41', '2018-09-17 22:34:41'),
(2, 'Yet again there is another, new method in quality assurance', 'Crowdtesting usually describes the fee-based testing of software, by a broad mass of free and independent tester through the internet. It is something comparable to an outsourced measure for quality assurance. And it can be used very flexibly, right from the very first click dummy in the process of software development. Primarily, the goal is to test application software on all relevant end devices and operating systems.\r\n\r\nThe term crowdtesting is based on the term crowdsourcing (crowd and outsourcing), which was introduced by Jeff Howe in his article “The Rise of Crowdsourcing” from 2006 [1]. There were first attempts to define the test method between 2009 and 2011, accompanied by an amalgamation of contributions to the topic.\r\n\r\nThe professional use of this test method as a service was developed at the end of 2010 and was met with approval in the start-up scene at the beginning. With the growing number of devices on the market, crowdtesting is becoming increasingly more important for companies developing application software, especially for the mobile development.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Panther - A browser testing and web crawling library for PHP and Symfony', 'Panther is a convenient standalone library to scrape websites and to run end-to-end tests using real browsers.\r\n\r\nPanther is super powerful, it leverages the W3C\'s WebDriver protocol to drive native web browsers such as Google Chrome and Firefox.\r\n\r\nPanther is very easy to use, because it implements the popular Symfony\'s BrowserKit and DomCrawler APIs, and contains all features you need to test your apps. It will sound familiar if you have ever created a functional test for a Symfony app: as the API is exactly the same! Keep in mind that Panther can be used in every PHP project, it\'s a standalone library.\r\n\r\nPanther automatically finds your local installation of Chrome and launches it (thanks to ChromeDriver), so you don\'t need to install anything on your computer, neither Selenium server nor obscure driver.\r\n\r\nIn test mode, Panther automatically starts your application using the PHP built-in web-server. You can just focus on writing your tests or web-scraping scenario, Panther takes care of everything else.', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', '$2y$10$yWzl8xwA13bBu4u1vopLk.EjuJevK1eWZKvJZBu3cTk.1X3eB/Giy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
