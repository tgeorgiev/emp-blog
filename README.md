## Project Request
Микроблог, който да има възможност за добавяне и изтриване на публикации, също така има опростен админ панел, в който се логват юзърите, за да публикуват/редактират.
- Технологии: PHP (съвместимо с 5.5 и нагоре), MySQL, jQuery. Angular или други по желание. 
- Framework: PHP SLIM FRAMEWORK 2.0 (инсталиран с PHP COMPOSER)

## Guidelines
-  Инсталира се PHP SLIM. Без рендер либ, но по желание може да се ползва PHP MUSTACHE или TWIG (отново инсталиран с PHP COMPOSER).
-  Създава се прост DataBase клас, който провежда необходимите заявки към базата (MySQL) или използвайки ORM.
-  Създава се User Class, в който се съдържат методите за аутентикация. По желание може да се добавят методи за създаване и изтриване на потребители. Добавя се метод, който да следи за изтекла сесия на аутентикацията (сесията може примерно да е 5 минути).
-  Създава се Posts Class, който служи за добавяне, изтриване и редактиране на публикации, като всяка публикация задължително трябва да има заглавие и съдържание(текст). Задължително е да постовете да се листват (в admin панела и front page-a).
-  PHPUnit тестове на създадените класове.

## Description
   The project is realised with Slim framework 2.0.

![project screenshot][screenshot_01]

**Static analysis of the project**
![project screenshot][screenshot_02]

## Installation
1. Clone the project from the repo
    ```
    git clone https://tgeorgiev@bitbucket.org/tgeorgiev/emp-blog.git
    ```
2. enter in the project `cd emp-blog`
3. setup a local vhost `http://emp-blog.local/`
4. execute `./emp-blog.sql` in MySQL to create DB `emp-blog`
5. run `composer install`
6. run `composer test`

## Notes
1. you can use username:`admin` and password:`admin` for login
2. you can see a video demo of the project on [youtube](https://youtu.be/-ASWqnyR_fo)
3. code coverage for the unit tests could be found at `reports\clover_html\index.html`


[screenshot_01]: https://bitbucket.org/tgeorgiev/emp-blog/raw/master/public/img/screenshot_01.png "Screenshot"
[screenshot_02]: https://bitbucket.org/tgeorgiev/emp-blog/raw/master/public/img/screenshot_02.png "Screenshot"