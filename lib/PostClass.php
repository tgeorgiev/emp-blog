<?php

namespace lib;

use PDO;

class PostClass extends DBClass
{
    protected $tableName = 'post';

    public function update($data)
    {
        if (empty($data['id']) || empty($data['title']) || empty($data['content'])) {
            return false;
        }

        $sql = "UPDATE {$this->tableName} SET title=:title, content=:content WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':id', $data['id']);
        $stmt->bindParam(':title', $data['title']);
        $stmt->bindParam(':content', $data['content']);

        return $stmt->execute();
    }

    public function add($data)
    {
        if (empty($data['title']) || empty($data['content'])) {
            return false;
        }

        $sql = "INSERT INTO {$this->tableName} (title, content) VALUES (:title, :content)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':title', $data['title']);
        $stmt->bindParam(':content', $data['content']);

        return $stmt->execute();
    }

    public function findLatest($limit = 5)
    {
        $stmt = $this->connection->prepare("SELECT * FROM {$this->tableName} LIMIT :limit");
        $stmt->bindParam(':limit', $limit);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}