<?php

namespace lib;

use PDO;

class DBClass
{
    private $dsn = "mysql:host=localhost;dbname=emp-blog;charset=utf8";
    private $user = 'root';
    private $pass = '';
    private $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];

    protected $connection;
    protected $tableName;

    public function __construct(PDO $connection = null)
    {
        $this->connection = $connection;
        if ($this->connection === null) {
            $this->connection = new PDO( $this->dsn, $this->user, $this->pass, $this->opt );
        }
    }

    public function find($id)
    {
        $stmt = $this->connection->prepare("SELECT * FROM {$this->tableName} WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function findAll()
    {
        $stmt = $this->connection->prepare("SELECT * FROM {$this->tableName}");
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function delete($id)
    {
        $stmt = $this->connection->prepare("DELETE FROM {$this->tableName} WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->execute();
    }
}