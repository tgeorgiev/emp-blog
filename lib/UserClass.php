<?php

namespace lib;

use PDO;

class UserClass extends DBClass
{
    protected $tableName = 'users';

    public function checkCredentials($data)
    {

        if (empty($data['user']) || empty($data['pass'])) {
            return false;
        }

        $user = $this->findByUsername($data['user']);

        if(!$user) {
            return false;
        }

        if(password_verify($data['pass'], $user->password)) {
            return true;
        }
        return false;
    }

    public function findByUsername($username)
    {
        $stmt = $this->connection->prepare("SELECT * FROM {$this->tableName} WHERE username = :username");
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function add($data)
    {
        if (empty($data['username']) || empty($data['password'])) {
            return false;
        }

        $sql = "INSERT INTO {$this->tableName} (username, password) VALUES (:username, :password)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':username', $data['username']);
        $stmt->bindParam(':password', $data['password']);

        return $stmt->execute();
    }

    public function update($data)
    {
        if (empty($data['id']) || empty($data['username'])) {
            return false;
        }

        // set username
        $sql = "UPDATE {$this->tableName} SET username=:username WHERE id=:id";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':id', $data['id']);
        $stmt->bindParam(':username', $data['username']);
        $stmt->execute();

        // change password if needed
        if(!empty($data['password'])) {
            $hash = password_hash($data['password'], PASSWORD_BCRYPT);
            $sql = "UPDATE {$this->tableName} SET password=:password WHERE id=:id";
            $stmt = $this->connection->prepare($sql);
            $stmt->bindParam(':id', $data['id']);
            $stmt->bindParam(':password', $hash);
            $stmt->execute();
        }
        return true;
    }

    public function findLatest($limit = 5)
    {
        $stmt = $this->connection->prepare("SELECT * FROM {$this->tableName} LIMIT :limit");
        $stmt->bindParam(':limit', $limit);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}