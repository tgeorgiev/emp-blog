<?php

use PHPUnit\Framework\TestCase;
use lib\UserClass;

class UserModelTest extends TestCase
{
    public function testFind()
    {
        $userDB = new UserClass();
        $users = $userDB->findAll();

        $this->assertEquals(count($users),1);

        $post = $userDB->find(1);

        $this->assertEquals($post->username,'admin');

        //$this->assertTrue($userDB->findByUsername('admin'));
        $this->assertEquals($userDB->findByUsername('admin')->username,'admin');

        $users = $userDB->findLatest(2);

        $this->assertEquals(count($users),1);
    }

    public function testCRUD()
    {
        $userDB = new UserClass();
        $data = array(
            'username' => 'demo',
            'password' => password_hash('demo', PASSWORD_BCRYPT)
        );

        $this->assertTrue($userDB->add($data));
        $users = $userDB->findAll();
        $lastUser = array_pop($users);
        $this->assertEquals($lastUser->username,'demo');

        $data = array(
            'user' => 'demo',
            'pass' => 'demo'
        );
        $this->assertTrue($userDB->checkCredentials($data));

        $data = array(
            'user' => 'demo',
            'pass' => 'wrong'
        );
        $this->assertFalse($userDB->checkCredentials($data));

        $data = array(
            'id' => $lastUser->id,
            'username' => 'demo2',
            'password' => 'demo2'
        );

        $this->assertTrue($userDB->update($data));
        $this->assertEquals($userDB->find($lastUser->id)->username,'demo2');

        $data = array(
            'user' => 'demo2',
            'pass' => 'demo2'
        );
        $this->assertTrue($userDB->checkCredentials($data));

        $this->assertTrue($userDB->delete($lastUser->id));
        $this->assertFalse($userDB->find($lastUser->id));
    }
}