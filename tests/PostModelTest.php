<?php

use PHPUnit\Framework\TestCase;
use lib\PostClass;

class PostModelTest extends TestCase
{
    public function testFind()
    {
        $postDB = new PostClass();
        $posts = $postDB->findAll();

        $this->assertEquals(count($posts),3);

        $post = $postDB->find(1);

        $this->assertEquals($post->title,'Bootstrap 4.1.2');

        $posts = $postDB->findLatest(2);

        $this->assertEquals(count($posts),2);
    }

    public function testCRUD()
    {
        $postDB = new PostClass();
        $data = array(
            'title' => 'demo title',
            'content' => 'demo content'
        );

        $this->assertTrue($postDB->add($data));
        $posts = $postDB->findAll();
        $lastPost = array_pop($posts);
        $this->assertEquals($lastPost->title,'demo title');

        $data = array(
            'id' => $lastPost->id,
            'title' => 'update title',
            'content' => 'update content'
        );

        $this->assertTrue($postDB->update($data));
        $this->assertEquals($postDB->find($lastPost->id)->title,'update title');
        $this->assertEquals($postDB->find($lastPost->id)->content,'update content');

        $this->assertTrue($postDB->delete($lastPost->id));
        $this->assertFalse($postDB->find($lastPost->id));
    }
}